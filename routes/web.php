<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserControllers;
use App\Http\Controllers\PosyanduController;
use App\Http\Controllers\ParentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
route :: middleware(['auth'])->group(function() {


Route::get('/', function () {
    return view('welcome');
});

Route::get('/user', [UserControllers::class, 'index']);
Route::post('/user', [UserControllers::class, 'store'])->name('user.store');
Route::post('/user/update', [UserControllers::class, 'update'])->name('user.update');

Route::get('/posyandu', [PosyanduController::class,'index']);
Route::post('/posyandu', [PosyanduController::class,'create'])->name('posyandu.create');
Route::post('/posyandu/update', [PosyanduController::class,'update'])->name('posyandu.update');


Route::get('/posyandu/provinces', [PosyanduController::class,'provinces']);
Route::get('/posyandu/cities', [PosyanduController::class,'cities']);
Route::get('/posyandu/districts', [PosyanduController::class,'districts']);
Route::get('/posyandu/villages', [PosyanduController::class,'villages']);


Route::get('/parent', [ParentController::class,'index']);
Route::get('/parent/create', [ParentController::class,'create']);
Route::post('/parent/', [ParentController::class,'store'])->name('parent.store');
Route::get('/parent/{id}', [ParentController::class,'detail']);

Route::get('/child', function () {
    return view('child.index');
});
Route::get('/child/create', function () {
    return view('child.create');
});
Route::get('/child/1', function () {
    return view('child.detail');
});
Route::get('/setting', function () {
    return view('setting.index');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
