<?php

use App\Http\Controllers\Api\Auth\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ServiceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', [RegisterController::class, 'index']);

//service
Route::get('/service/provinces', [ServiceController::class, 'provinces']);
Route::get('/service/cities', [ServiceController::class, 'cities']);
Route::get('/service/districts', [ServiceController::class, 'districts']);
Route::get('/service/villages', [ServiceController::class, 'villages']);
Route::get('/service/posyandus', [ServiceController::class, 'posyandus']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
