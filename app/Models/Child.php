<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    use HasFactory;
    protected $table = 'childs';

    public function village(){
        return $this->belongsTo(Village::class, 'village_id', 'id');
    }
    public function parent(){
        return $this->belongsTo(Parents::class, 'parent_id', 'id');
    }
}
