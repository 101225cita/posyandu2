<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportKms extends Model
{
    protected $table = 'report_kms';
    use HasFactory;

    public function child(){
        return $this->belongsTo(Child::class, 'child_id', 'id');
    }
}
