<?php

namespace App\Http\Controllers;

use App\Models\Posyandu;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('posyandu')->get();
        $posyandus = Posyandu::orderBy('name', 'ASC')->get();
        return view('user.index', [
            'user' => $user,
            'posyandus' => $posyandus
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users', 'max:255'],
            'posyandu_id' => ['required', 'numeric', 'exists:posyandus,id'],
            'role' => ['required', 'string', 'in:bidan,kader'],
            'phone' => ['required', 'numeric', 'digits_between:6,14'],
            'password' => ['required', 'min:8', 'max:32']
        ]);

        $user = new User();
        $user['name'] = $data['name'];
        $user['email'] = $data['email'];
        $user['posyandu_id'] = $data['posyandu_id'];
        $user['role'] = $data['role'];
        $user['phone'] = $data['phone'];
        $user['password'] = hash::make($data['password']);
        $user['status'] = 'ACTIVE';
        $user->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'posyandu_id' => ['required', 'numeric', 'exists:posyandus,id'],
            'role' => ['required', 'string', 'in:Bidan,Kader'],
            'phone' => ['required', 'numeric', 'digits_between:6,14'],
            'status' => ['required', 'string', 'in:Active,NonActive'],
        ]);

        $user = User::where('id', $request['id']) -> first();
        $user['name'] = $data['name'];
        $user['email'] = $data['email'];
        $user['posyandu_id'] = $data['posyandu_id'];
        $user['role'] = $data['role'];
        $user['phone'] = $data['phone'];
        $user['status'] = $data['status'];
        
        $user->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
