<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Posyandu;
use Illuminate\Http\Request;
use Indonesia;

class ServiceController extends Controller
{
    public function provinces()
    {
        $provinces = (Indonesia::allProvinces())->sortBy('name')->values();
        return response()->json($provinces,200);
    }
    public function cities(Request $request)
    {
        try{
            $data = $request->validate([
                'province_id' => ['required', 'numeric']
            ]);
            $province_id = $data['province_id'];
            $province_with_cities = Indonesia::findProvince($province_id, ['cities']);
            $cities = $province_with_cities['cities'];
            $cities = collect($cities)->sortBy('name')->values();
            return response()->json($cities,200);
        } catch (\Throwable $th) {
            return response()->json([],200);
        }
    }

    public function districts(Request $request)
    {
        try{
            $data = $request->validate([
                'city_id' => ['required', 'numeric']
            ]);
            $city_id = $data['city_id'];
            $city_with_ditricts = Indonesia::findCity($city_id, ['districts']);
            $districts = $city_with_ditricts['districts'];
            $districts = collect($districts)->sortBy('name')->values();
            return response()->json($districts,200);
        }catch(\Throwable $th){
            return response()->json([],200);
        }
    }
    public function villages(Request $request)
    {
        try{
            $data = $request->validate([
                'district_id' => ['required', 'numeric']
            ]);
            $district_id = $data['district_id'];
            $district_with_villages = Indonesia::findDistrict($district_id, ['villages']);
            $villages = $district_with_villages['villages'];
            $villages = collect($villages)->sortBy('name')->values();
            return response()->json($villages,200);
        }catch(\Throwable $th){
            return response()->json([],200);
        }
    }

    public function posyandus(Request $request)
    {
        try {
            $data = $request->validate([
                'village_id' => ['required', 'numeric'],
            ]);
            $posyandus = Posyandu::where('village_id', $data['village_id'])->get();
            return response()->json($posyandus,200);
        } catch (\Throwable $th) {
            return response()->json([],200);
        }
    }
}
