<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Parents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'place_of_birth' => ['required', 'string', 'max:255'],
            'date_of_birth' => ['required', 'date', 'date_format:Y-m-d'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'digits_between:6,14', 'unique:parents,phone'],
            'password' => ['required', 'string', 'max:255'],
            'province_id' => ['required', 'numeric', 'exists:indonesia_provinces,id'],
            'city_id' => ['required', 'numeric', 'exists:indonesia_cities,id'],
            'district_id' => ['required', 'numeric', 'exists:indonesia_districts,id'],
            'village_id' => ['required', 'numeric', 'exists:indonesia_villages,id'],
            'posyandu_id' => ['required', 'numeric', 'exists:posyandus,id']
        ]);

        $password = Hash::make($data['password']);
        $api_token = Str::random(80);

        $parent = new Parents();
        $parent['name'] = $data['name'];
        $parent['place_of_birth'] = $data['place_of_birth'];
        $parent['date_of_birth'] = $data['date_of_birth'];
        $parent['address'] = $data['address'];
        $parent['phone'] = $data['phone'];
        $parent['password'] = $password;
        $parent['province_id'] = $data['province_id'];
        $parent['city_id'] = $data['city_id'];
        $parent['district_id'] = $data['district_id'];
        $parent['village_id'] = $data['village_id'];
        $parent['posyandu_id'] = $data['posyandu_id'];
        $parent['api_token'] = $api_token;
        $parent->save();

        return response()->json([
            'parent' =>$parent,
                'meta'=> [
                    'api_token' => $api_token
                ]
                ], 201);
    }


}
