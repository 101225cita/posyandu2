<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Indonesia;
use PhpParser\Node\Stmt\TryCatch;
use App\Models\Posyandu;

class PosyanduController extends Controller
{
    public function index()
    {
        return view('posyandu.index', [
            'posyandus' => Posyandu::latest()->get(),
            'provinces' => $this->provinces()
        ]);
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'province' => ['required', 'numeric', 'exists:indonesia_provinces,id'],
            'city' => ['required', 'numeric', 'exists:indonesia_cities,id'],
            'district' => ['required', 'numeric', 'exists:indonesia_districts,id'],
            'village' => ['required', 'numeric', 'exists:indonesia_villages,id'],
            'pic_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits_between:6,14']
        ]);

        $posyandu = new Posyandu();
        $posyandu['name'] = $data['name'];
        $posyandu['province_id'] = $data['province'];
        $posyandu['city_id'] = $data['city'];
        $posyandu['district_id'] = $data['district'];
        $posyandu['village_id'] = $data['village'];
        $posyandu['phone'] = $data['phone'];
        $posyandu['pic_name'] = $data['pic_name'];
        $posyandu->save();

        return redirect()->back();
    }

    public function provinces()
    {
        return collect(Indonesia::allProvinces())->sortBy('name')->values();
    }
    public function cities(Request $request)
    {
        try{
            $data = $request->validate([
                'province_id' => ['required', 'numeric']
            ]);
            $province_id = $data['province_id'];
            $province_with_cities = Indonesia::findProvince($province_id, ['cities']);
            $cities = $province_with_cities['cities'];
            return collect($cities)->sortBy('name')->values();
        } catch (\Throwable $th) {
            return [];
        }
    }

    public function districts(Request $request)
    {
        try{
            $data = $request->validate([
                'city_id' => ['required', 'numeric']
            ]);
            $city_id = $data['city_id'];
            $city_with_ditricts = Indonesia::findCity($city_id, ['districts']);
            $districts = $city_with_ditricts['districts'];
            return collect($districts)->sortBy('name')->values();
        }catch(\Throwable $th){
            return[];
        }
    }
    public function villages(Request $request)
    {
        try{
            $data = $request->validate([
                'district_id' => ['required', 'numeric']
            ]);
            $district_id = $data['district_id'];
            $district_with_villages = Indonesia::findDistrict($district_id, ['villages']);
            $villages = $district_with_villages['villages'];
            return collect($villages)->sortBy('name')->values();
        }catch(\Throwable $th){
            return[];
        }
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'province' => ['required', 'numeric', 'exists:indonesia_provinces,id'],
            'city' => ['required', 'numeric', 'exists:indonesia_cities,id'],
            'district' => ['required', 'numeric', 'exists:indonesia_districts,id'],
            'village' => ['required', 'numeric', 'exists:indonesia_villages,id'],
            'pic_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits_between:6,14']
        ]);

        $posyandu = Posyandu::where('id', $request['id'])->first();
        $posyandu['name'] = $data['name'];
        $posyandu['province_id'] = $data['province'];
        $posyandu['city_id'] = $data['city'];
        $posyandu['district_id'] = $data['district'];
        $posyandu['village_id'] = $data['village'];
        $posyandu['phone'] = $data['phone'];
        $posyandu['pic_name'] = $data['pic_name'];
        $posyandu->save();

        return redirect()->back();
    }
}
