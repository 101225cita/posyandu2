@extends('layouts.navigation')
@section('title', 'User')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List User</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="button" class="btn btn-primary" style="margin-bottom: 10px;" data-toggle="modal"
                            data-target="#modalAddUser">Add User</button>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>No Hp</th>
                                    <th>Posyandu</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($user as $item)
                                <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['email'] }}</td>
                                <td>{{ $item['role'] }}</td>
                                <td>{{ $item['phone'] }}</td>
                                <td>{{ $item['posyandu']['name'] }}</td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#modalUpdateUser"
                                            data-id="{{ $item['id'] }}"
                                            data-name="{{ $item['name'] }}"
                                            data-email="{{ $item['email'] }}"
                                            data-role="{{ $item['role'] }}"
                                            data-phone="{{ $item['phone'] }}"
                                            data-status="{{ $item['status'] }}"
                                            data-posyandu-id="{{ $item['posyandu']['id'] }}"
                                            id="editUser"
                                            >Edit
                                            User</button>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            User</button> --}}
                                    </td>
                                </tr>
                                @empty
                                     <tr>
                                     <td colspan="7">
                                      <p style="text-align: center">Data tidak ditemukan</p>

                                        </td>
                                        </tr>
                                @endforelse

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modalAddUser" tabindex="-1" role="dialog" aria-labelledby="modalAddUserLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add User</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('user.store') }}"method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="{{ old('phone') }}">
                        @error('name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        @error('email')
                            <span class='text-danger'>{{ $message }}</span>

                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Enter email">
                         @error('password')
                            <span class='text-danger'>{{ $message }}</span>

                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="phone" name="phone"
                            placeholder="Enter phone number" value="{{ old('phone') }}">
                         @error('phone')
                            <span class='text-danger'>{{ $message }}</span>

                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select class="form-control" id="role" name="role">
                            <option value="Bidan" {{ old('role') == 'Bidan' ? 'selected' : '' }}>Bidan</option>
                            <option value="Kader" {{ old('role') == 'Bidan' ? 'selected' : '' }}>Kader</option>
                        </select>
                        @error('role')
                            <span class='text-danger'>{{ $message }}</span>

                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="posyandu">Posyandu</label>
                        <select class="form-control" id="posyandu" name="posyandu_id">
                            @foreach ($posyandus as $posyandu)
                                <option value="{{ $posyandu['id']}}"
                                {{ old('posyandu_id') === $posyandu['id'] ? 'selected' : ''}}>
                                    {{ $posyandu['name']}}
                                </option>
                            @endforeach
                        @error('posyandu_id')
                            <span class='text-danger'>{{ $message }}</span>

                        @enderror


                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalUpdateUser" tabindex="-1" role="dialog" aria-labelledby="modalUpdateUserLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update User</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('user.update') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="hidden" class="form-control" id="updateId" name="id" placeholder="Enter name">
                        <input type="text" class="form-control" id="updateName" name="name" placeholder="Enter name">
                        @error('name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="updateEmail" name="email" placeholder="Enter email">
                        @error('email')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="updatePhone" name="phone"
                            placeholder="Enter phone number">
                        @error('phone')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select class="form-control" id="updateRole" name="role">
                            <option value="Bidan" id="Bidan">Bidan</option>
                            <option value="Kader" id="Kader">Kader</option>
                        </select>
                        @error('role')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="posyandu">Posyandu</label>
                        <select class="form-control" id="updatePosyandu" name="posyandu_id">
                        @foreach ($posyandus as $posyandu)
                                <option value="{{ $posyandu['id']}}"
                                id="{{ $posyandu['id']}}">
                                    {{ $posyandu['name'] }}
                                </option>
                            @endforeach
                        </select>
                        @error('posyandu_id')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="updateStatus" name="status">
                            <option value="Active" id="Active">Aktif</option>
                            <option value="Nonactive" id="Nonactive">Non Aktif</option>
                        </select>
                        @error('status')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#userTable').DataTable({
          columnDefs: [{
              "defaultContent": "-",
              "targets": "_all"
          }
          ]
      });
    });

    $(document).on('click', '#editUser', function(){

        $('#updateName').val($(this).attr('data-name'));
        $('#updateId').val($(this).attr('data-id'));
        $('#updateEmail').val($(this).attr('data-email'));
        $('#updatePhone').val($(this).attr('data-phone'));

        if ($(this).attr('data-role') === 'Bidan') {
            document.getElementById('Bidan').selected = "true";
        } else if ($(this).attr('data-role') === 'Kader') {
            document.getElementById('Kader').selected = "true";
        }
        

        if ($(this).attr('data-status') === 'Active') {
            document.getElementById('Active').selected = "true";
        } else if ($(this).attr('data-status') === 'Nonactive') {
            document.getElementById('Nonactive').selected = "true";
        }
        document.getElementById($(this).attr('data-posyandu-id')).selected = "true";

    });
</script>
@endsection
