@extends('layouts.navigation')
@section('title', 'Posyandu')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Posyandu
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Posyandu</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Posyandu</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="button" class="btn btn-primary" style="margin-bottom: 10px;" data-toggle="modal"
                            data-target="#modalAddPosyandu">Add Posyandu</button>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Desa</th>
                                    <th>Kecamatan</th>
                                    <th>Kabupaten</th>
                                    <th>Ketua Posyandu</th>
                                    <th>No HP</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($posyandus as $posyandu)
                                <tr>
                                <td>{{ $posyandu['id'] }}</td>
                                <td>{{ $posyandu['name'] }}</td>
                                <td>{{ $posyandu->village->name ?? '-' }}</td>
                                <td>{{ $posyandu->district->name ?? '-' }}</td>
                                <td>{{ $posyandu->city->name ?? '-' }}</td>
                                <td>{{ $posyandu['pic_name'] }}</td>
                                <td>{{ $posyandu['phone'] }}</td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal"
                                        data-id="{{ $posyandu['id'] }}"
                                        data-name = "{{ $posyandu['name'] }}"
                                        data-province-id = "{{ $posyandu->province->id }}"
                                        data-village-id = "{{ $posyandu->village->id }}"
                                        data-district-id = "{{ $posyandu->district->id }}"
                                        data-city-id = "{{ $posyandu->city->id }}"
                                        data-pic-name = "{{ $posyandu['pic_name'] }}"
                                        data-phone = "{{ $posyandu['phone'] }}"
                                        id="updatePosyandu"
                                        data-target="#modalUpdatePosyandu">
                                            Edit Posyandu</button>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            Posyandu</button> --}}
                                    </td>
                                </tr>
                                @empty
                                     <tr>
                                     <td colspan="7">
                                      <p style="text-align: center">Data tidak ditemukan</p>

                                        </td>
                                        </tr>
                                @endforelse
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modalAddPosyandu" tabindex="-1" role="dialog" aria-labelledby="modalAddPosyanduLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Posyandu</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('posyandu.create') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama">
                        @error('name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <select name="province" id="province_nested" class="form-control">
                            <option value="">Pilih Provinsi</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province['id'] }}">
                                    {{ $province['name'] }}
                                </option>

                            @endforeach
                        </select>
                        @error('province')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <select name="city" id="city_nested" class="form-control">
                            <option value="">Pilih Kabupaten/Kota</option>
                        </select>
                        @error('city')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <select name="district" id="district_nested" class="form-control">
                            <option value="">Pilih Kecamatan</option>
                        </select>
                        @error('district')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <select name="village" id="village_nested" class="form-control">
                            <option value="">Pilih Desa</option>
                        </select>
                        @error('village')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pic_name">Nama Ketua</label>
                        <input type="text" class="form-control" id="pic_name" name="pic_name"
                            placeholder="Masukan Nama Ketua">
                                @error('pic_name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">Nomor HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Masukan Nomor HP">
                        @error('phone')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalUpdatePosyandu" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePosyanduLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Posyandu</h4>
            </div>
            <div class="modal-body">
            <form method="POST" action="{{ route('posyandu.update') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="hidden" class="form-control" id="updateId" name="id">
                        <input type="text" class="form-control" id="updateName" name="name" placeholder="Masukan Nama">
                        @error('name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <select name="province" id="province_nested_update" class="form-control">
                            <option value="">Pilih Provinsi</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province['id'] }}" id="{{ $province['id'] }}">
                                    {{ $province['name'] }}
                                </option>

                            @endforeach
                        </select>
                        @error('province')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <select name="city" id="city_nested_update" class="form-control">
                            <option value="">Pilih Kabupaten/Kota</option>
                        </select>
                        @error('city')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <select name="district" id="district_nested_update" class="form-control">
                            <option value="">Pilih Kecamatan</option>
                        </select>
                        @error('district')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <select name="village" id="village_nested_update" class="form-control">
                            <option value="">Pilih Desa</option>
                        </select>
                        @error('village')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pic_name">Nama Ketua</label>
                        <input type="text" class="form-control" id="update_pic_name" name="pic_name"
                            placeholder="Masukan Nama Ketua">
                                @error('pic_name')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">Nomor HP</label>
                        <input type="text" class="form-control" id="updatePhone" name="phone" placeholder="Masukan Nomor HP">
                        @error('phone')
                            <span class='text-danger'>{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#userTable').DataTable()
    })

//untuk add
    $('#province_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/cities', {
            province_id:val
        }, function(data,textStatus, jqXHR) {
            $('#city_nested').empty()
            $('#district_nested').empty()
            $('#village_nested').empty()
            addFirstOption('province')
            $.each(data, function(index, data){
                $('#city_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    $('#city_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/districts', {
            city_id:val
        }, function(data,textStatus, jqXHR) {
            $('#district_nested').empty()
            $('#village_nested').empty()
            addFirstOption('city')
            $.each(data, function(index, data){
                $('#district_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    $('#district_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/villages', {
            district_id:val
        }, function(data,textStatus, jqXHR) {
            $('#village_nested').empty()
            addFirstOption('district')
            $.each(data, function(index, data){
                $('#village_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    function addFirstOption(params) {
        if (params == 'province') {
            $('#city_nested').append($('<option>', {
                    text:'Pilih Kabupaten/Kota'
            }))
            $('#district_nested').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'city') {
            $('#district_nested').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'district') {
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))
        }
    }

//untuk update

    $('#province_nested_update').change(function() {
        var val = $(this).val()
        $.get('/posyandu/cities', {
            province_id:val
        }, function(data,textStatus, jqXHR) {
            $('#city_nested_update').empty()
            $('#district_nested_update').empty()
            $('#village_nested_update').empty()
            addFirstOptionUpdate('province')
            $.each(data, function(index, data){
                $('#city_nested_update').append($('<option>', {
                    value: data.id,
                    text: data.name,
                    id : data.id
                }))
            })
        })
    })

    $('#city_nested_update').change(function() {
        var val = $(this).val()
        $.get('/posyandu/districts', {
            city_id:val
        }, function(data,textStatus, jqXHR) {
            $('#district_nested_update').empty()
            $('#village_nested_update').empty()
            addFirstOptionUpdate('city')
            $.each(data, function(index, data){
                $('#district_nested_update').append($('<option>', {
                    value: data.id,
                    text:data.name,
                    id : data.id
                }))
            })
        })
    })

    $('#district_nested_update').change(function() {
        var val = $(this).val()
        $.get('/posyandu/villages', {
            district_id:val
        }, function(data,textStatus, jqXHR) {
            $('#village_nested_update').empty()
            addFirstOptionUpdate('district')
            $.each(data, function(index, data){
                $('#village_nested_update').append($('<option>', {
                    value: data.id,
                    text:data.name,
                    id : data.id
                }))
            })
        })
    })

    function addFirstOptionUpdate(params) {
        if (params == 'province') {
            $('#city_nested_update').append($('<option>', {
                    text:'Pilih Kabupaten/Kota'
            }))
            $('#district_nested_update').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested_update').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'city') {
            $('#district_nested_update').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested_update').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'district') {
            $('#village_nested_update').append($('<option>', {
                    text:'Pilih Desa'
            }))
        }
    }

    $(document).on('click', '#updatePosyandu', function(){

$('#updateName').val($(this).attr('data-name'));
$('#updateId').val($(this).attr('data-id'));
$('#updatePhone').val($(this).attr('data-phone'));
$('#update_pic_name').val($(this).attr('data-pic-name'));

var province = $(this).attr('data-province-id')
var village = $(this).attr('data-village-id')
var district = $(this).attr('data-district-id')
var city = $(this).attr('data-city-id')

document.getElementById(province).selected = "true";

        $.get('/posyandu/cities', {
            province_id: province
        }, function(data,textStatus, jqXHR) {
            $('#city_nested_update').empty()
            $('#district_nested_update').empty()
            $('#village_nested_update').empty()
            addFirstOptionUpdate('province')
            $.each(data, function(index, data){
                $('#city_nested_update').append($('<option>', {
                    value: data.id,
                    text: data.name,
                    id : data.id
                }))
            })
            document.getElementById(city).selected = "true";
        })

        $.get('/posyandu/districts', {
            city_id:city
        }, function(data,textStatus, jqXHR) {
            $('#district_nested_update').empty()
            $('#village_nested_update').empty()
            addFirstOptionUpdate('city')
            $.each(data, function(index, data){
                $('#district_nested_update').append($('<option>', {
                    value: data.id,
                    text:data.name,
                    id : data.id
                }))
            })
            document.getElementById(district).selected = "true";
        })

        $.get('/posyandu/villages', {
            district_id:district
        }, function(data,textStatus, jqXHR) {
            $('#village_nested_update').empty()
            addFirstOptionUpdate('district')
            $.each(data, function(index, data){
                $('#village_nested_update').append($('<option>', {
                    value: data.id,
                    text:data.name,
                    id : data.id
                }))
            })
            document.getElementById(village).selected = "true";
        })
});

</script>
@endsection
