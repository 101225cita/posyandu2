@extends('layouts.navigation')
@section('title', 'Child')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Child
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/">Child</a></li>
            <li class="active">Detail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-child"></i> Bukan Anak Baim
                    <small class="pull-right">Date: 2/10/2014</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <strong>Desa</strong>
                <address>
                    Ajibarang
                </address>
                <strong>Orang Tua</strong>
                <address>
                    Baim
                </address>
                <strong>Tempat, Tanggal Lahir</strong>
                <address>
                    Ajibarang, 1 Januari 2021
                </address>
                <strong>Jenis Kelamin</strong>
                <address>
                    Laki - Laki
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>No Kartu Keluarga</strong>
                <address>
                    123456789
                </address>
                <strong>Nik Anak</strong>
                <address>
                    123456789
                </address>
                <strong>KIA</strong>
                <address>
                    123456789
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Nama Ayah</strong>
                <address>
                    Abim
                </address>
                <strong>NIK Ayah</strong>
                <address>
                    123456789
                </address>
                <strong>Nama Ibu</strong>
                <address>
                    Farah
                </address>
                <strong>NIK Ibu</strong>
                <address>
                    123456789
                </address>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <button type="button" class="btn btn-info pull-right" style="margin-right: 5px;">
                    <i class="fa fa-pencil"></i> Edit
                </button>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li class="active"><a href="#kmsReport" data-toggle="tab">KMS</a></li>
                        <li><a href="#sales-chart" data-toggle="tab">Imunisasi dan Vitamin</a></li>
                        <li class="pull-left header"><i class="fa fa-inbox"></i> Laporan</li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="kmsReport" style="position: relative; height: 300px;">
                            <!-- /.nav-tabs-custom -->
                            <div class="box-body">
                                <button type="button" data-toggle="modal" data-target="#modalAddKms"
                                    class="btn btn-primary" style="margin-bottom: 10px;">Add
                                    Report</button>
                                <table id="kmsReportTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Periksa</th>
                                            <th>BB</th>
                                            <th>TB</th>
                                            <th>LK</th>
                                            <th>Usia</th>
                                            <th>Status</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1 Januari 2021</td>
                                            <td>23</td>
                                            <td>23</td>
                                            <td>23</td>
                                            <td>23</td>
                                            <td>Aktif</td>
                                            <td>Aman Sehat</td>
                                        </tr>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                            <!-- /.nav-tabs-custom -->
                            <div class="box-body">
                                <button type="button" data-toggle="modal" data-target="#modalAddImun" class=" btn
                                    btn-primary" style="margin-bottom: 10px;">Add
                                    Report</button>
                                <table id="imunReportTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Periksa</th>
                                            <th>Kategori</th>
                                            <th>Jenis</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1 Januari 2021</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                            <td>Lorem Ipsum</td>
                                        </tr>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modalAddKms" tabindex="-1" role="dialog" aria-labelledby="modalAddKmsLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Report KMS</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="bb">BB</label>
                        <input type="text" class="form-control" id="bb" name="bb" placeholder="Enter bb">
                    </div>
                    <div class="form-group">
                        <label for="tb">TB</label>
                        <input type="text" class="form-control" id="tb" name="tb" placeholder="Enter tb">
                    </div>
                    <div class="form-group">
                        <label for="lk">LK</label>
                        <input type="text" class="form-control" id="lk" name="lk" placeholder="Enter lk">
                    </div>
                    <div class="form-group">
                        <label for="notes">Catatan</label>
                        <input type="text" class="form-control" id="notes" name="notes" placeholder="Enter notes">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalAddImun" tabindex="-1" role="dialog" aria-labelledby="modalAddImunLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Immunization and Vitamin Report</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="category">Kategori</label>
                        <input type="text" class="form-control" id="category" name="category"
                            placeholder="Enter category">
                    </div>
                    <div class="form-group">
                        <label for="type">Jenis</label>
                        <input type="text" class="form-control" id="type" name="type" placeholder="Enter type">
                    </div>
                    <div class="form-group">
                        <label for="notes">Catatan</label>
                        <input type="text" class="form-control" id="notes" name="notes" placeholder="Enter notes">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#kmsReportTable').DataTable({
      });
      $('#imunReportTable').DataTable({
      });
    });
</script>
@endsection