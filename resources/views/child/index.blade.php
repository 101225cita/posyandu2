@extends('layouts.navigation')
@section('title', 'Child')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Child
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Child</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Child</h3>
                    </div>
                    <div class="box-body">
                        <a href="/child/create" class="btn btn-primary" style="margin-bottom: 10px;">Add Child</a>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Anak</th>
                                    <th>Jenis Kelamin</th>
                                    <th>TTL</th>
                                    <th>No KIA</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Baim</td>
                                    <td>Laki Laki</td>
                                    <td>Ajibarang, 1 Januari 2021</td>
                                    <td>12345678910</td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="/child/1">Detail</a>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            Child</button> --}}
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#userTable').DataTable({
      });
    });
</script>
@endsection