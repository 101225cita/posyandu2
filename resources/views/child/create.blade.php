@extends('layouts.navigation')
@section('title', 'Child')
@section('css')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Child
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/child">Child</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Create Child</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- form start -->
                        <form role="form">
                            <div class="form-group">
                                <label for="village">Pilih Desa</label>
                                <select class="form-control" id="village" name="village">
                                    <option>Desa</option>
                                    <option>Desa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="parent">Nama Orangtua</label>
                                <select class="form-control" id="parent" name="parent">
                                    <option>Orangtua</option>
                                    <option>Orangtua</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="childName">Nama Lengkap Anak</label>
                                <input type="email" class="form-control" id="childName" id="childName"
                                    placeholder="Enter child name">
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="birtPlace">Tempat Lahir</label>
                                        <input type="email" class="form-control" id="birtPlace" id="birtPlace"
                                            placeholder="Enter birth place">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="datepicker">Tanggal Lahir</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                name="birthDate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="gender" id="gender" value="male" checked="">
                                    Laki Laki
                                </label>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <label>
                                    <input type="radio" name="gender" id="gender" value="female">
                                    Perempuan
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="familyCardNumber">No Kartu Keluarga</label>
                                <input type="email" class="form-control" id="familyCardNumber" id="familyCardNumber"
                                    placeholder="Enter family card number">
                            </div>
                            <div class="form-group">
                                <label for="nik">NIK Anak</label>
                                <input type="email" class="form-control" id="nik" id="nik" placeholder="Enter nik">
                            </div>
                            <div class="form-group">
                                <label for="fatherName">Nama Ayah</label>
                                <input type="email" class="form-control" id="fatherName" id="fatherName"
                                    placeholder="Enter father name">
                            </div>
                            <div class="form-group">
                                <label for="fatherNik">NIK Ayah</label>
                                <input type="email" class="form-control" id="fatherNik" id="fatherNik"
                                    placeholder="Enter father nik">
                            </div>
                            <div class="form-group">
                                <label for="motherName">Nama Ibu</label>
                                <input type="email" class="form-control" id="motherName" id="motherName"
                                    placeholder="Enter mother name">
                            </div>
                            <div class="form-group">
                                <label for="motherNik">NIK Ibu</label>
                                <input type="email" class="form-control" id="motherNik" id="motherNik"
                                    placeholder="Enter mother nik">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('js')
<!-- bootstrap datepicker -->
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
@endsection