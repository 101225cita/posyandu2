@extends('layouts.navigation')
@section('title', 'Parent')
@section('css')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parent
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Parent</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Parent</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- form start -->
                        <form action="{{ route('parent.store') }}" method="POST" role="form">
                            @csrf
                            <div class="box-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="mothersName">Nama Lengkap Ibu</label>
                                        <input type="text" class="form-control" id="mothersName" name="mothersName"
                                            placeholder="Enter mother's name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="province">Provinsi</label>
                                        <select name="province" id="province_nested" class="form-control">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach ($provinces as $province)
                                            <option value="{{ $province['id'] }}" id="{{ $province['id'] }}">
                                                {{ $province['name'] }}
                                            </option>
                                            @endforeach
                                            </select>
                                            @error('province')
                                                <span class='text-danger'>{{ $message }}</span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="birthPlace">Tempat Lahir</label>
                                        <input type="text" class="form-control" id="birthPlace" name="birthPlace"
                                            placeholder="Enter birth place">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="birthDate">Tanggal Lahir</label>
                                            <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                name="birthDate">
                                            </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city">Kabupaten / Kota</label>
                                        <select name="city" id="city_nested" class="form-control">
                                        <option value="">Pilih Kabupaten/Kota</option>
                                        </select>
                                        @error('city')
                                            <span class='text-danger'>{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="address">Alamat</label>
                                        <input type="text" class="form-control" id="address" name="address"
                                            placeholder="Enter address">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="districts">Kecamatan</label>
                                        <select name="district" id="district_nested" class="form-control">
                                            <option value="">Pilih Kecamatan</option>
                                        </select>
                                        @error('district')
                                            <span class='text-danger'>{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="phone">No HP</label>
                                            <input type="text" class="form-control" id="phone" name="phone"
                                                            placeholder="Enter phone">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="village">Desa</label>
                                            <select name="village" id="village_nested" class="form-control">
                                                <option value="">Pilih Desa</option>
                                            </select>
                                            @error('village')
                                                <span class='text-danger'>{{ $message }}</span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password"
                                            placeholder="Enter Password">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="posyandu">Posyandu</label>
                                        <select class="form-control" id="posyandu" name="posyandu_id">
                                            @foreach ($posyandus as $posyandu)
                                                <option value="{{ $posyandu['id']}}"
                                                {{ old('posyandu_id') === $posyandu['id'] ? 'selected' : ''}}>
                                                    {{ $posyandu['name']}}
                                                </option>
                                            @endforeach
                                            @error('posyandu_id')
                                                <span class='text-danger'>{{ $message }}</span>
                                            @enderror
                                        </select>
                                    </div>
                                </div>
                            </div>
                                        <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('js')
<!-- bootstrap datepicker -->
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    $('#province_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/cities', {
            province_id:val
        }, function(data,textStatus, jqXHR) {
            $('#city_nested').empty()
            $('#district_nested').empty()
            $('#village_nested').empty()
            addFirstOption('province')
            $.each(data, function(index, data){
                $('#city_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    $('#city_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/districts', {
            city_id:val
        }, function(data,textStatus, jqXHR) {
            $('#district_nested').empty()
            $('#village_nested').empty()
            addFirstOption('city')
            $.each(data, function(index, data){
                $('#district_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    $('#district_nested').change(function() {
        var val = $(this).val()
        $.get('/posyandu/villages', {
            district_id:val
        }, function(data,textStatus, jqXHR) {
            $('#village_nested').empty()
            addFirstOption('district')
            $.each(data, function(index, data){
                $('#village_nested').append($('<option>', {
                    value: data.id,
                    text:data.name
                }))
            })
        })
    })

    function addFirstOption(params) {
        if (params == 'province') {
            $('#city_nested').append($('<option>', {
                    text:'Pilih Kabupaten/Kota'
            }))
            $('#district_nested').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'city') {
            $('#district_nested').append($('<option>', {
                    text:'Pilih Kecamatan'
            }))
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))

        } else if (params == 'district') {
            $('#village_nested').append($('<option>', {
                    text:'Pilih Desa'
            }))
        }
    }
</script>
@endsection