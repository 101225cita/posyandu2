@extends('layouts.navigation')
@section('title', 'Parent')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parent
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Parent</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Parent</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <a href="/parent/create" class="btn btn-primary" style="margin-bottom: 10px;">Add Parent</a>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Desa</th>
                                    <th>Posyandu</th>
                                    <th>No HP</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($parents as $parent)
                                 <tr>
                                    <td>{{ $parent['id']}}</td>
                                    <td>{{ $parent['name']}}</td>
                                    <td>{{ $parent['address']}}</td>
                                    <td>{{ $parent->village->name ?? '-' }}</td>
                                    <td>{{ $parent['posyandu']['name']}}</td>
                                    <td>{{ $parent['phone']}}</td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="/parent/{{ $parent['id'] }}">Detail</a>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            Parent</button> --}}
                                    </td>
                                </tr>
                                @endforeach
                               
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modalAddParent" tabindex="-1" role="dialog" aria-labelledby="modalAddParentLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Parent</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <input type="text" class="form-control" id="province" name="province"
                            placeholder="Enter province">
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <input type="text" class="form-control" id="district" name="district"
                            placeholder="Enter district">
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <input type="text" class="form-control" id="village" name="village" placeholder="Enter village">
                    </div>
                    <div class="form-group">
                        <label for="chairmanName">Nama Ketua</label>
                        <input type="text" class="form-control" id="chairmanName" name="chairmanName"
                            placeholder="Enter chairman name">
                    </div>
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalUpdateParent" tabindex="-1" role="dialog" aria-labelledby="modalUpdateParentLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Parent</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <input type="text" class="form-control" id="province" name="province"
                            placeholder="Enter province">
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <input type="text" class="form-control" id="district" name="district"
                            placeholder="Enter district">
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <input type="text" class="form-control" id="village" name="village" placeholder="Enter village">
                    </div>
                    <div class="form-group">
                        <label for="chairmanName">Nama Ketua</label>
                        <input type="text" class="form-control" id="chairmanName" name="chairmanName"
                            placeholder="Enter chairman name">
                    </div>
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#userTable').DataTable({
      });
    });
</script>
@endsection