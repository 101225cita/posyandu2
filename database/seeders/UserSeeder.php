<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Cita',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'posyandu_id' => 1,
            'role' => 'Bidan',
            'phone' => '085329401737',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posyandus')->insert([
            'name' => 'Posyandu 1',
            'village_id' => 32057,
            'district_id' => 2657,
            'city_id' => 189,
            'province_id' => 13,
            'pic_name' => 'Bu Yuni',
            'phone' => '085329401737',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
